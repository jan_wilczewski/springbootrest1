//package springboot1.repository;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.repository.query.Param;
//import org.springframework.data.rest.core.annotation.RepositoryRestResource;
//import springboot1.model.Task;
//
//import java.util.List;
//
//@RepositoryRestResource
//public interface TaskRepository extends JpaRepository<Task, Integer> {
//
//    List<Task> findByTaskAchieved(@Param("achievedfalse") int taskAchievedFalse);
//    List<Task> findByTaskStatus(@Param("status") String taskStatus);
//
//}
